from telegram import ReplyKeyboardMarkup, Update
from telegram.ext import (CommandHandler, CallbackContext, DispatcherHandlerStop)

from config import ADMIN

keyboard = ReplyKeyboardMarkup([
    [
        'Send Message',
        'Get Backup',
    ],
    [
        'HOME',
    ]
])


def start_dev(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id

    if not (chat_id in ADMIN):
        context.bot.send_message(chat_id, 'whomst has awakened the ancient one')
        raise DispatcherHandlerStop

    context.bot.send_message(chat_id, 'Hello dear Developer!', reply_markup=keyboard)


HANDLER = CommandHandler('dev', start_dev)
