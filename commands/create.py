from telegram import Update, ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import CommandHandler, CallbackContext, ConversationHandler, MessageHandler, Filters

from config import ADMIN
from models import *

NAME = range(1)


end_keyboard = ReplyKeyboardMarkup([
    [
        'Add Song',
    ],
    [
        'Rename Playlist',
        'Delete Playlist',
    ],
    [
        'Create Playlist',
        'Get Playlist',
    ],
    [
        'HOME',
    ],
])


def check_name_exist(name, chat_id):
    user = User.select().where(User.chat_id == chat_id)[0]
    playlist = Playlist.select().where((Playlist.name == name) & (Playlist.user == user))
    if len(playlist):
        return False
    return True


def start_create(update: Update, context: CallbackContext):
    # clear user data
    context.user_data.clear()

    chat_id = update.message.from_user.id

    try:
        if len(context.args):
            name = context.args[0]

            # check if name already exists
            if check_name_exist(name, chat_id):
                context.bot.send_message(chat_id, 'This name is already exists!')
                raise

            context.user_data['name'] = name

            create_playlist(update, context)

            return ConversationHandler.END
    except:
        pass

    context.bot.send_message(chat_id,
                             ('Enter name of the playlist you want to create!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=ReplyKeyboardRemove())
    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check if name already exists
    if not check_name_exist(name, chat_id):
        context.bot.send_message(chat_id, 'Sorry but this name is already exists :((')
        return

    context.user_data['name'] = name

    return create_playlist(update, context)


def create_playlist(update, context):
    chat_id = update.message.from_user.id

    name = context.user_data['name']

    try:
        Playlist.insert({
            Playlist.name: name,
            Playlist.user: User.select().where(User.chat_id == chat_id),
        }).execute()

        context.bot.send_message(chat_id,
                                 ('OK!\n' +
                                  'Playlist *{}* successfully created!\n'.format(name)),
                                 parse_mode='Markdown',
                                 reply_markup=end_keyboard)

    except Exception as err:
        if chat_id in ADMIN:
            context.bot.send_message(chat_id, err)
        return error(update, context)

    context.user_data.clear()

    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('create', start_create),
        MessageHandler(Filters.text('Create Playlist'), start_create),
    ],

    states={

        NAME: [MessageHandler(Filters.text, get_name)],

    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
