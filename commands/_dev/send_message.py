from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import (ConversationHandler, MessageHandler, CommandHandler, Filters, DispatcherHandlerStop,
                          CallbackContext)

from config import ADMIN

from models import User

CHAT_ID, MESSAGE = range(2)

end_keyboard = ReplyKeyboardMarkup([
    [
        'Send Message',
        'Get Backup',
    ],
    [
        'HOME',
    ]
])


def start_send_message(update: Update, context: CallbackContext):
    context.user_data.clear()

    chat_id = update.message.from_user.id

    if not (chat_id in ADMIN):
        context.bot.send_message(chat_id, 'whomst has awakened the ancient one')
        raise DispatcherHandlerStop

    context.bot.send_message(chat_id, ('Send chat_id:\n' +
                                       '0 for send to all'), reply_markup=ReplyKeyboardRemove())

    return CHAT_ID


def get_chat_id(update, context):
    chat_id = update.message.from_user.id

    try:
        receiver = int(update.message.text)
    except:
        context.bot.send_message(chat_id, 'Invalid input! send again:')
        return

    context.bot.send_message(chat_id, 'Send your message:')

    context.user_data['receiver'] = receiver

    return MESSAGE


def get_message(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id
    message = update.message.message_id
    receiver = context.user_data['receiver']

    if receiver:
        try:
            context.bot.forward_message(receiver, chat_id, message)
            context.bot.send_message(chat_id, 'Success :)', reply_markup=end_keyboard)
            return ConversationHandler.END
        except:
            context.bot.send_message(chat_id, 'Failed :(', reply_markup=end_keyboard)
            return ConversationHandler.END
    for user in User.select():
        try:
            context.bot.forward_message(user.chat_id, chat_id, message)
        except:
            pass

    context.bot.send_message(chat_id, 'Message was sent to everyone successfully!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('send_message', start_send_message),
        MessageHandler(Filters.text('Send Message'), start_send_message),
    ],

    states={
        CHAT_ID: [MessageHandler(Filters.text, get_chat_id)],

        MESSAGE: [MessageHandler(Filters.all, get_message)],
    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
