from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import (CommandHandler, CallbackContext, DispatcherHandlerStop, MessageHandler,
                          Filters, ConversationHandler)

from config import ADMIN


keyboard = ReplyKeyboardMarkup([
    [
        'Send Message',
        'Get Backup',
    ],
    [
        'HOME',
    ]
])


def get_backup(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id

    if not (chat_id in ADMIN):
        context.bot.send_message(chat_id, 'whomst has awakened the ancient one', reply_markup=ReplyKeyboardRemove())
        raise DispatcherHandlerStop

    try:
        doc = open('database.sqlite3', 'rb')
        context.bot.send_document(chat_id, doc, reply_markup=keyboard)
    except:
        context.bot.send_message(chat_id, 'Something went wrong! Try again:', reply_markup=keyboard)

    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('get_backup', get_backup),
        MessageHandler(Filters.text('Get Backup'), get_backup)
    ],

    states={

    },

    fallbacks=[

    ],

    allow_reentry=True,
)
