from datetime import datetime

from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove, ChatAction
from telegram.ext import CommandHandler, CallbackContext, ConversationHandler, MessageHandler, Filters

from models import Playlist, Song, User, History


NAME = range(1)


end_keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
    ],
    [
        'Create Playlist',
    ],
    [
        'HOME',
    ],
])


def check_name_exist(name, chat_id):
    user = User.select().where(User.chat_id == chat_id)[0]
    playlist = Playlist.select().where((Playlist.name == name) & (Playlist.user == user))
    if len(playlist):
        return True
    return False


def start_get(update: Update, context: CallbackContext):
    context.user_data.clear()

    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)[0]
    playlists = Playlist.select().where(Playlist.user == user)

    if not len(playlists):
        context.bot.send_message(chat_id, 'You should create playlist first =)', reply_markup=end_keyboard)
        return ConversationHandler.END

    keyboard = ReplyKeyboardMarkup([[playlist.name] for playlist in playlists], resize_keyboard=True)

    try:
        if len(context.args):
            name = context.args[0]

            # check playlist exists!
            if not check_name_exist(name, chat_id):
                context.bot.send_message(chat_id, 'Sorry :((\n Playlist with this name does not exists! Enter again:',
                                         reply_markup=keyboard)
                return ConversationHandler.END

            context.user_data['name'] = name

            return get_playlist(update, context)
    except:
        pass

    context.bot.send_message(chat_id,
                             ('Enter the name of the playlist you want to listen!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=keyboard)
    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check playlist exists
    if not check_name_exist(name, chat_id):
        context.bot.send_message(chat_id,
                                 'Sorry but playlist with this name does not exists :(( Enter again:')
        return

    context.user_data['name'] = name

    return get_playlist(update, context)


def clear_history(update, context):
    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)[0]
    history = History.select().where(History.user == user)

    for message in history:
        try:
            context.bot.delete_message(chat_id, message.message_id)
        except:
            pass

    History.delete().where(History.user == user).execute()


def get_playlist(update, context):
    chat_id = update.message.from_user.id

    name = context.user_data['name']

    clear_history(update, context)

    user = User.select().where(User.chat_id == chat_id)[0]
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))[0]
    songs = Song.select().where(Song.playlist == playlist)

    context.bot.send_chat_action(chat_id, ChatAction.UPLOAD_AUDIO)

    for song in songs:
        message = context.bot.send_audio(chat_id, song.file_id)
        History.insert({
            History.user: user,
            History.date: datetime.now(),
            History.message_id: message.message_id,
            History.file_id: song.file_id
        }).execute()

    context.bot.send_message(chat_id, 'Here you are!\nenjoy ^__^', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('get', start_get),
        MessageHandler(Filters.text('Get Playlist'), start_get),
    ],

    states={

        NAME: [MessageHandler(Filters.regex(r'^([a-z]|[A-Z]|\s)+$'), get_name)],

    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
