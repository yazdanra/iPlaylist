from telegram import Update, ReplyKeyboardMarkup, ChatAction, ReplyKeyboardRemove
from telegram.ext import CallbackContext, ConversationHandler, CommandHandler, MessageHandler, Filters

from config import ADMIN
from models import User, Song, Playlist


NAME, SONG = range(2)


done_keyboard = ReplyKeyboardMarkup([
    [
        'Done',
    ],
    [
        'Cancel',
    ]
])

end_keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
    ],
    [
        'Create Playlist',
    ],
    [
        'HOME',
    ],
])


def check_playlist_exists(update, context, name):
    chat_id = update.message.from_user.id

    user = User.select().where(User.chat_id == chat_id)
    playlist = Playlist.select().where((Playlist.user == user) & (Playlist.name == name))
    if not len(playlist):
        return True

    return False


def start_add_song(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id

    try:
        if len(context.args):
            name = context.args[0]

            if check_playlist_exists(update, context, name):
                context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
                return ConversationHandler.END

            context.bot.send_message(chat_id,
                                     ('Now send me your songs!\n' +
                                      'next send `Done` to add songs to *{}*!\n\n'.format(name) +
                                      '/cancel to cancel the process'),
                                     parse_mode='Markdown',
                                     reply_matkup=done_keyboard)

            context.user_data['name'] = name
            context.user_data['songs'] = []

            return SONG

    except:
        pass

    user = User.select().where(User.chat_id == chat_id)[0]
    playlists = Playlist.select().where(Playlist.user == user)

    keyboard = ReplyKeyboardMarkup([[playlist.name] for playlist in playlists], resize_keyboard=True)

    context.bot.send_message(chat_id,
                             ('Enter the name of the playlist you wanna add song!\n\n' +
                              '/cancel to cancel the process'),
                             reply_markup=keyboard)

    return NAME


def get_name(update, context):
    chat_id = update.message.from_user.id
    name = update.message.text

    # check playlist exists
    if check_playlist_exists(update, context, name):
        context.bot.send_message(chat_id, 'This Playlist dose not exist! Enter again:')
        return

    context.bot.send_message(chat_id,
                             ('Now send me your songs!\n' +
                              'next send `Done` to add songs to *{}*!\n\n'.format(name) +
                              '/cancel to cancel the process'),
                             parse_mode='Markdown',
                             reply_markup=done_keyboard)

    context.user_data['name'] = name
    context.user_data['songs'] = []

    return SONG


def get_song(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id
    name = context.user_data['name']

    try:
        song = context.bot.getFile(update.message.audio)
    except:
        context.bot.delete_message(chat_id, update.message.message_id)
        context.bot.send_message(chat_id, 'Please send a song!')
        return

    try:
        user = User.select().where(User.chat_id == chat_id)[0]

        # add song to playlist
        Song.insert({
            Song.title: update.message.audio.performer + " " + update.message.audio.title,
            Song.file_id: song.file_id,
            Song.file_path: song.file_path,
            Song.file_size: song.file_size,
            Song.playlist: Playlist.select().where((Playlist.user == user) & (Playlist.name == name))[0]
        }).execute()
        context.user_data['songs'].append(update.message)

    except Exception as err:
        if chat_id in ADMIN:
            context.bot.send_message(chat_id, err)
        context.bot.send_message(chat_id, 'Oops...\nSomething went wrong! Try again:')


def done(update, context: CallbackContext):
    chat_id = update.message.from_user.id
    name = context.user_data['name']

    context.bot.send_message(chat_id, 'This step may take few second! :)')
    context.bot.send_chat_action(chat_id, ChatAction.UPLOAD_AUDIO)

    # delete songs from user
    for message in context.user_data['songs']:
        context.bot.delete_message(message.chat.id, message.message_id)

    context.bot.send_message(chat_id,
                             '*{}* songs added to *{}* successfully!'.format(
                                 len(context.user_data['songs']),
                                 name),
                             reply_markup=end_keyboard,
                             parse_mode='Markdown')

    context.user_data.clear()

    return ConversationHandler.END


def error(update, context):
    chat_id = update.message.chat.id
    context.bot.delete_message(chat_id, update.message.message_id)
    context.bot.send_message(chat_id, ('Oops...\n' +
                                       'Something went wrong! Try again:'))
    return


def cancel(update, context):
    chat_id = update.message.from_user.id
    context.bot.send_message(chat_id, 'Process was canceled!', reply_markup=end_keyboard)

    context.user_data.clear()
    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('add', start_add_song),
        MessageHandler(Filters.text('Add Song'), start_add_song),
    ],

    states={

        NAME: [MessageHandler(Filters.text, get_name)],

        SONG: [
            MessageHandler(Filters.audio, get_song),
            MessageHandler(Filters.text('Done'), done)
        ],

    },

    fallbacks=[
        MessageHandler(Filters.command, cancel),
        MessageHandler(Filters.text('Cancel'), cancel),
        MessageHandler(Filters.all, error)
    ],

    allow_reentry=True,
)
