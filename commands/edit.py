from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import CommandHandler, CallbackContext, ConversationHandler, MessageHandler, Filters

keyboard = ReplyKeyboardMarkup([
    [
        'Add Song',
        'Remove Song',
    ],
    [
        'Rename Playlist',
        'Delete Playlist',
    ],
    [
        'HOME',
    ],
])


def start_edit(update: Update, context: CallbackContext):
    context.user_data.clear()
    chat_id = update.message.from_user.id

    context.bot.send_message(chat_id, 'Choose one!', reply_markup=keyboard)

    return ConversationHandler.END


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('edit', start_edit),
        MessageHandler(Filters.text('Edit Playlist'), start_edit),
    ],

    states={

    },

    fallbacks=[

    ],

    allow_reentry=True,
)
