from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import CommandHandler, CallbackContext, ConversationHandler, MessageHandler, Filters


keyboard = ReplyKeyboardMarkup([
    [
        'Get Playlist',
    ],
    [
        'Create Playlist',
        'Edit Playlist',
    ],
    [
        'Contact us!',
    ],
    [
        'Support us!',
    ],
])


def say_hello(update: Update, context: CallbackContext):
    context.user_data.clear()

    chat_id = update.message.chat.id
    name = update.message.from_user.first_name

    context.bot.send_message(chat_id,
                             ('Hello {}!\n'.format(name) +
                              'Welcome to playlist bot! you can use commands or keyboard\n' +
                              'enjoy it :)\n'),
                             parse_mode='Markdown',
                             reply_markup=keyboard)


def home(update, context):
    context.user_data.clear()

    chat_id = update.message.chat.id
    name = update.message.from_user.first_name

    context.bot.send_message(chat_id, 'What can i do for you {}'.format(name),
                             parse_mode='Markdown', reply_markup=keyboard)


HANDLER = ConversationHandler(
    entry_points=[
        CommandHandler('start', say_hello),
        CommandHandler('home', home),
        MessageHandler(Filters.regex(r'(HOME)'), home),
    ],

    states={

    },

    fallbacks=[

    ],

    allow_reentry=True,
)
