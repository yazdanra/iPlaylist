import logging
import os

from telegram.ext import CallbackContext, Updater, MessageHandler, Filters


from commands import (greetings, create, get, edit, support, contact, developer)
from commands._dev import (send_message, get_backup)
from commands._edit import (add, rename, remove, delete)

from models import *


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def load_environment():
    from dotenv import load_dotenv
    load_dotenv()

    # OR, the same with increased verbosity:
    load_dotenv(verbose=True)

    # OR, explicitly providing path to '.env'
    from pathlib import Path  # python3 only
    env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path)


load_environment()

database.init(os.getenv("DATABASE"))
database.connect()
database.create_tables([
    User,
    Message,
    Playlist,
    Song,
    History,
])


def save_data(update, context):
    chat_id = update.message.from_user.id
    username = update.message.from_user.username
    first_name = update.message.from_user.first_name
    last_name = update.message.from_user.last_name
    is_bot = update.message.from_user.is_bot
    language_code = update.message.from_user.language_code
    text = update.message.text
    details = str(update)

    user = User.select().where(User.chat_id == chat_id)

    if not len(user):
        User.insert({
            User.first_name: first_name,
            User.last_name: last_name,
            User.username: username,
            User.chat_id: chat_id,
            User.is_bot: is_bot,
            User.language_code: language_code,
        }).execute()
    if len(user):
        User.update({
            User.first_name: first_name,
            User.last_name: last_name,
            User.username: username,
            User.chat_id: chat_id,
            User.is_bot: is_bot,
            User.language_code: language_code,
        }).where(User.chat_id == chat_id).execute()

    Message.insert({
        Message.chat_id: chat_id,
        Message.username: username,
        Message.text: text,
        Message.details: details,
    }).execute()


DATA = MessageHandler(Filters.all, save_data)


def clear_history_time(context: CallbackContext):
    for h in History.select():
        user = User.select().join(History).where(History.user_id == User.id)[0]

        chat_id = user.chat_id
        message_id = h.message_id

        delta = (datetime.now() - h.date)
        seconds = delta.seconds

        if seconds >= 20 * 60 * 60:
            try:
                context.bot.delete_message(chat_id, message_id)
                context.bot.send_message(chat_id, 'Enjoy ^__^')
                message = context.bot.send_audio(chat_id, h.file_id)

                History.insert({
                    History.user: user,
                    History.date: datetime.now(),
                    History.message_id: message.message_id,
                    History.file_id: h.file_id
                }).execute()

                History.delete().where(History.id == h.id).execute()
            except:
                pass


def main():
    # Start bot
    updater = Updater(os.getenv("TOKEN"), use_context=True)

    bot = updater.dispatcher
    job = updater.job_queue
    job_minute = job.run_repeating(clear_history_time, interval=20 * 60 * 60, first=0)

    ### BOT HANDLERS

    # DATA
    bot.add_handler(DATA, group=0)

    # DEVELOPER
    bot.add_handler(developer.HANDLER, group=1)
    bot.add_handler(send_message.HANDLER, group=2)
    bot.add_handler(get_backup.HANDLER, group=3)

    # GREETINGS
    bot.add_handler(greetings.HANDLER, group=5)

    # EDITS
    bot.add_handler(edit.HANDLER, group=6)
    bot.add_handler(add.HANDLER, group=7)
    bot.add_handler(rename.HANDLER, group=8)
    bot.add_handler(remove.HANDLER, group=9)
    bot.add_handler(delete.HANDLER, group=10)

    # OTHERS
    bot.add_handler(create.HANDLER, group=11)
    bot.add_handler(get.HANDLER, group=12)
    bot.add_handler(support.HANDLER, group=13)
    bot.add_handler(contact.HANDLER, group=14)

    # Start polling
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
